﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Centipede
{
    public static class ClearPlayerPefs
    {
        [MenuItem("Tools/Reset PlayerPrefs")]
        public static void DeletePlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }
    }  
}
