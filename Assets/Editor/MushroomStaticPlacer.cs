﻿using UnityEditor;
using UnityEngine;

namespace Centipede
{
    [InitializeOnLoadAttribute]
    public static class MushroomStaticPlacer
    {
        private static MushroomsPlacer mushroomPlacer;

        static MushroomStaticPlacer()
        {
            EditorApplication.hierarchyChanged += OnHierarchyChanged;
        }

        static void OnHierarchyChanged()
        {
            if (EditorApplication.isPlaying)
                return;

            if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Mushroom>() != null)
            {
                mushroomPlacer = GameObject.Find("MushroomsController").GetComponent<MushroomsPlacer>();

                if (!Selection.activeGameObject.activeInHierarchy)
                    return;

                if (mushroomPlacer.PlacerType == MushroomPlacementType.Static)
                {
                    Utils.Init();

                    var mushrooms = mushroomPlacer.Place(Selection.activeGameObject);

                    if (mushrooms == null || mushrooms.Count == 0)
                        return;

                    var mushroom = mushrooms[0];

                    foreach (GameObject go in Object.FindObjectsOfType(typeof(GameObject)))
                    {
                        if ((go.transform.position == mushroom.Position) && (go != mushroom.gameObject) && (go.CompareTag(Tags.MushroomTag)))
                        {
                            Debug.LogErrorFormat("Mushroom can't be placed at position {0}, because GameObject {1} already exist at the position",
                                mushroom.Position, go.name);

                            GameObject.DestroyImmediate(mushroom.gameObject);
                            return;
                        }
                    }
                }
                else if (mushroomPlacer.PlacerType == MushroomPlacementType.Random)
                {
                    Debug.LogError("<b>You can place mushrooms statically by dropping mushroom prefabs only in Static mode</b>");
                    GameObject.DestroyImmediate(Selection.activeGameObject);
                }
            }
        }
    }
}