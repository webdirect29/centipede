﻿using UnityEngine;

namespace Centipede
{
    public class CentipedeMoveBounds : ScreenBounds
    {
        [HideInInspector]
        public int PassedStepsInd;

        public override float BottomBound => Utils.TopBound - Utils.StepSize * PassedStepsInd;
    }
}

