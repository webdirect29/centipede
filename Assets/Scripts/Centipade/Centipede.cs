﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Centipede
{
    public class Centipede
    {
        private List<CentipedeElement> elements;

        public CentipedeElement Head => elements[0];
        public int Length => elements.Count;

        public bool Alive => elements.Count > 0;

        public Centipede(CentipedeMover moveController, MushroomsController mushController)
        {
            elements = new List<CentipedeElement>();
            moveController.Centipade = this;
        }

        public CentipedeElement AddElement(CentipedeElement element)
        {
            if (elements.Count > 0)
                element.Previous = elements[elements.Count - 1];

            elements.Add(element);
            element.name += elements.Count;
            return element;
        }

        public CentipedeElement RemoveElement(CentipedeElement element)
        {
            if (elements.Count == 0)
                return null;

            CentipedeElement removeElement = null;

            if (element.IsHead)
                removeElement = element;
            else
                removeElement = elements[elements.Count - 1];

            elements.Remove(removeElement);
            return removeElement;
        }

        public Vector3 Position
        {
            get { return Head.Position; }
            set { Head.Position = value; }
        }

        public Quaternion Rotation
        {
            get { return Head.Rotation; }
            set { Head.Rotation = value; }
        }

        public Transform Transform => Head.transform;

        public void Move(Vector3 position, float moveSpeed, TweenCallback afterMoveCallback)
        {
            for (int i = elements.Count - 1; i >= 0; i--)
            {
                if (elements[i].IsHead)
                    continue;

                elements[i].Move(position, moveSpeed, afterMoveCallback);
            }
            Head.Move(position, moveSpeed, afterMoveCallback);
        }

        public void Rotate(Vector3 euler)
        {
            Head.Rotate(euler);
        }

        public void RotateAll(Vector3 euler)
        {
            foreach (var element in elements)
            {
                element.Rotation = Quaternion.Euler(euler);
            }
        }

        public bool IsCrushed
        {
            get
            {
                if (elements.Count <= 1)
                    return false;

                for (int i = 1; i < elements.Count; i++)
                {
                    if (Head.Position == elements[i].Position)
                        return true;
                }
                return false;
            }
        }
    }
}