﻿using System.Collections.Generic;
using UnityEngine;

namespace Centipede
{
    public class CentipedeController : GameElementController
    {
        [SerializeField]
        private GameObject centipedePrefab;

        [SerializeField]
        private Transform centipedeParent;

        [SerializeField]
        private CentipedeMover mover;

        [SerializeField]
        private MushroomsController mushroomsController;

        [SerializeField, Range(1, 6)]
        private int lenght = 1;

        [SerializeField, Header("Graphics")]
        private Sprite headSpr;

        [SerializeField]
        private Sprite tailSpr;

        public Centipede Centipede { get; private set; } //Centipende consists of several CentipadeElements
        public bool Exist => Centipede != null;

        public override void Init()
        {
            mover.MushroomsController = mushroomsController;
            Create();
        }

        private void Create()
        {
            Centipede = new Centipede(mover, mushroomsController);            

            var yOffset = -1; //Position beyound the screen to start movement from
            var placePosition = CalculatePlacementPosition(yOffset);

            for (int i = 0; i < lenght; i++) //Create a body of Centipede
            {
                var element = AddElement();
                element.Position = new Vector3(placePosition.x + i * Utils.StepSize, placePosition.y);
            }
            mover.CentipedeBounds.PassedStepsInd = 0;
            mover.StartMovement();
        }

        private void SetGraphics(CentipedeElement centipedeElement)
        {
            centipedeElement.SpriteRenderer.sprite = centipedeElement.IsHead ? headSpr : tailSpr;
        }

        private CentipedeElement AddElement()
        {
            var element = Centipede.AddElement(CentipedeElementCreator.Create(centipedePrefab, centipedeParent, headSpr, tailSpr));
            SetGraphics(element);
            element.SpriteRenderer.sprite = element.IsHead ? headSpr : tailSpr;
            element.Dead += OnDead;
            return element;
        }

        private void OnDead(Creature creature)
        {
            var removeElement = Centipede.RemoveElement(creature as CentipedeElement);
            removeElement.Dead -= OnDead;
            var position = removeElement.Position;

            Destroy(removeElement.gameObject);
            Destroy(removeElement);

            if (!removeElement.IsHead)
            {
                mushroomsController.CreateMushroomWithoutRestrictions(position);
                return;
            }

            if (Centipede.Alive)
            {
                //Change a Centipede head
                Centipede.Head.Previous = null;
                SetGraphics(Centipede.Head);
                mover.StartMovement();
            }
            else 
            {
                //All pieces of Centipede died. Create a new one Centipede
                Centipede = null;
                Init();
                mover.IncreaseSpeed();
            }
        }

        private Vector3 CalculatePlacementPosition(int yOffset)
        {
            var possiblePositions = new List<Vector3>();

            for (int colomn = 0; colomn < Utils.ColomnsCount; colomn++)
                possiblePositions.Add(Utils.CalculatePlacementPosition(colomn, yOffset));

            return possiblePositions[Utils.GetRandomIntValue(0, possiblePositions.Count - 1)];
        }
    }
}
