﻿using UnityEngine;

namespace Centipede
{
    public static class CentipedeElementCreator
    {
        public static CentipedeElement Create(GameObject prefab, Transform parent, Sprite headSpr, Sprite tailSpr)
        {
            var element = Utils.CreateObjecOfType<CentipedeElement>(prefab, Vector3.zero, parent, prefab.name);
            element.SpriteRenderer.sprite = headSpr;
            return element;
        }
    }
}
