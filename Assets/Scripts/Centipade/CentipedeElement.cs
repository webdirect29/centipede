﻿using System;
using UnityEngine;
using DG.Tweening;

namespace Centipede
{
    public class CentipedeElement : Creature //The part of the centipede
    {
        protected override int InitHealth => 1;

        public CentipedeElement Previous;
        public bool IsHead => Previous == null;

        public void Rotate(Vector3 euler)
        {
            transform.Rotate(euler);
        }

        public void Move(Vector3 position, float moveSpeed, TweenCallback afterMoveCallback)
        {
            var speed = .1f / moveSpeed;
            var easyType = Ease.Linear;

            if (IsHead)
            {
                transform.DOMove(position, speed).OnComplete(afterMoveCallback).SetEase(easyType);   
            }
            else
            {               
                transform.DOMove(Previous.Position, speed).SetEase(easyType);
                Rotation = Previous.Rotation;
            }
        }
    }
}
