﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Centipede
{
    public class CentipedeMover : MoveController
    {
        [SerializeField, Range(1.1f, 2f)]
        public float MoveSpeedIncreaseRatio = 1.1f;

        private HashSet<Vector3> positions; //Positions on what centipede could make a step

        private Vector2 moveDir;
        private Vector2 lastMoveDir;

        private Vector2 leftMoveDir = new Vector2(-1, 0);
        private Vector2 rightMoveDir = new Vector2(1, 0);
        private Vector2 moveDownDir = new Vector2(0, -1);

        private bool IsLeftMove => moveDir == leftMoveDir;
        private bool IsRightMove => moveDir == rightMoveDir;
        private bool IsDownMove => moveDir == moveDownDir;

        private bool leftOverlapsed;
        private bool rightoverlapsed;

        protected override bool CanMove => Centipade != null;

        public Centipede Centipade;

        [HideInInspector]
        public MushroomsController MushroomsController;

        #if UNITY_EDITOR
        void OnDrawGizmosSelected()
        {
            DrawPlacementPositions();
        }

        private void DrawPlacementPositions()
        {
            Gizmos.color = Color.yellow;
            Utils.Init();

            foreach (var pos in Utils.CalculateStepPositions(bounds))
                Gizmos.DrawWireSphere(pos, 0.01f);
        }
        #endif

        protected override void Init()
        {
            if (positions == null || positions.Count == 0)
                positions = Utils.CalculateStepPositions(bounds);
        }

        protected override void Move(Vector3 position)
        {
            //Check a possible collisions of centipede with screen and obstacles and make a changes of it direction
            if ((bounds.OverlapsLeft(position.x)) && IsLeftMove)
            {
                leftOverlapsed = true;
                OnOverlapsHorizontal();
            }
            else if ((bounds.OverlapsRight(position.x)) && IsRightMove)
            {
                rightoverlapsed = true;
                OnOverlapsHorizontal();
            }
            else if ((bounds.OverlapsBottom(position.y)) && IsDownMove)
            {
                OnOverlapsBottom();
            }

            TweenCallback afterMoveCallback = () =>
            {
                if (CanMove)
                    MoveToNextPositon();
            };

            //Centipede crashed itseft. Make a restart
            if (Centipade.IsCrushed && GameController.State != GameState.Over)
            {
                GameController.Restart();
            }

            Centipade.Move(position, moveSpeed, afterMoveCallback);
        }

        private Vector2 GetRandomDirection()
        {
            var result = Utils.GetRandomIntValue(0, 1);
            return result == 0 ? leftMoveDir : rightMoveDir;
        }

        public void StartMovement()
        {
            Init();
            SetMovement(GetRandomDirection());
            RotateTowards(-moveDir.x, true);
            MoveToNextPositon();
        }

        private void SetMovement(Vector2 dir)
        {
            lastMoveDir = moveDir;
            moveDir = dir;
        }

        protected override Vector3 CalculateMovePosition()
        {
            var position = Utils.GetClosestPosition(Centipade.Position, positions, moveDir);
            var isObstacle = MushroomsController.IsPlaced(position);

            if (!isObstacle)
                return position;

            if (IsLeftMove || IsRightMove)
                OnOverlapsHorizontal();
            else
                OnOverlapsBottom();

            return CalculateMovePosition();
        }

        private void OnOverlapsHorizontal()
        {
            CentipedeBounds.PassedStepsInd++;
            RotateTowards(-moveDir.x);
            SetMovement(moveDownDir);
        }

        private void OnOverlapsBottom()
        {
            var dir = lastMoveDir == leftMoveDir ? rightMoveDir : leftMoveDir;
            RotateTowards(dir.x);
            SetMovement(dir);

            if (IsGoalAchieved)
            {
                GameController.GameOver();
            }
        }

        public bool IsGoalAchieved => Utils.RawsCount - 1 <= CentipedeBounds.PassedStepsInd;

        private void RotateTowards(float rotateTo, bool rotateAll = false, float step = 90)
        {
            RotateTowards((int)rotateTo, rotateAll, step);
        }

        private void RotateTowards(int rotateTo, bool rotateAll = false, float step = 90)
        {
            RotateEuler(new Vector3(0, 0, rotateTo * step), rotateAll);
        }

        public void RotateEuler(Vector3 euler, bool rotateAll = false)
        {
            if (rotateAll)
                Centipade.RotateAll(euler);
            else
                Centipade.Rotate(euler);
        }

        public void IncreaseSpeed()
        {
            moveSpeed *= MoveSpeedIncreaseRatio;
        }

        public CentipedeMoveBounds CentipedeBounds => bounds as CentipedeMoveBounds;
    }
}
