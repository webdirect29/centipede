﻿using UnityEngine;

namespace Centipede
{
    public abstract class GameElementController : MonoBehaviour
    {
        public abstract void Init();
    }
}