﻿using System;
using UnityEngine;

namespace Centipede
{
    public abstract class Creature : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private ColisionDetector colisionDetector;

        protected int health;

        public SpriteRenderer SpriteRenderer => spriteRenderer;

        public event Action<Creature> HealthChanged;
        public event Action<Creature> Dead;

        protected abstract int InitHealth { get; }

        private void Awake()
        {
            Health = InitHealth;
            colisionDetector.Collided += OnCollided;
        }

        private void OnDestroy()
        {
            colisionDetector.Collided -= OnCollided;
        }

        protected virtual void OnCollided(Collider2D collider)
        {
            if (collider.CompareTag(Tags.BulletTag))
            {
                var bullet = collider.GetComponent<Bullet>();

                if (bullet)
                {
                    Health -= bullet.Damage;
                    bullet.Destroy();
                }
            }
        }

        public int Health
        {
            get { return health; }

            set
            {
                health = value;

                if (health <= 0)
                {
                    Dead?.Invoke(this);
                    return;
                }
                HealthChanged?.Invoke(this);
            }
        }

        public Vector3 Position
        {
            get { return transform.position; }
            set { transform.position = value; }
        }

        public Quaternion Rotation
        {
            get { return transform.rotation; }
            set { transform.rotation = value; }
        }
    }
}
