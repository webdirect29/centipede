﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Centipede
{
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private List<GameElementController> controlers;

        public static GameState State { get; private set; }
        public static event Action<GameState> GameStateChanged;

        private void Awake()
        {
            controlers.ForEach(c => c.Init());
            State = GameState.Playing;
        }

        public static void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public static void GameOver()
        {
            State = GameState.Over;
            GameStateChanged?.Invoke(State);
        }
    }
}