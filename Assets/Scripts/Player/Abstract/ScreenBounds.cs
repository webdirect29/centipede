﻿using UnityEngine;

namespace Centipede
{
    public abstract class ScreenBounds : MonoBehaviour //Possible restrictions for some specific object to move or instantiate objects
    {
        public virtual float TopBound => Utils.TopBound;
        public virtual float BottomBound => Utils.BottomBound;
        public virtual float RightBound => Utils.RightBound;
        public virtual float LeftBound => Utils.LeftBound;

        public bool OverlapsTop(float value) => value >= TopBound;
        public bool OverlapsBottom(float value) => value <= BottomBound;
        public bool OverlapsRight(float value) => value >= RightBound;
        public bool OverlapsLeft(float value) => value <= LeftBound;

        public bool OverlapsPosition(Vector3 position)
        {
            return OverlapsTop(position.y) ||
                   OverlapsBottom(position.y) ||
                   OverlapsRight(position.x) ||
                   OverlapsLeft(position.x);
        }

        public bool OverlapsPositionWithOffset(Vector3 position)
        {
            return OverlapsTop(position.y - Utils.ScreenVerticalOffset) ||
                   OverlapsBottom(position.y + Utils.ScreenVerticalOffset) ||
                   OverlapsRight(position.x - Utils.ScreenHorizontalOffset) ||
                   OverlapsLeft(position.x + Utils.ScreenHorizontalOffset);
        }
    }
}
