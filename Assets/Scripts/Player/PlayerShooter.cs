﻿using UnityEngine;

namespace Centipede
{
    public class PlayerShooter : MonoBehaviour
    {
        [SerializeField, Range(0.1f, 1)]
        private float fireDelay;

        [SerializeField]
        private GameObject bulletPrefab;

        [SerializeField]
        private Transform bulletsParent;

        [SerializeField, Range(1, 20)]
        private float fireSpeed = 5;

        [SerializeField,Range(1, 10)]
        private float bulletAutodestroyTime = 2f;

        private float fireDelayTimer;

        private bool CanShoot => fireDelayTimer <= 0;
        private bool NeedShoot => Input.GetButton("Fire1") && CanShoot;

        private void Update()
        {
            if (NeedShoot)
            {
                Fire();
                fireDelayTimer = fireDelay;
            }

            fireDelayTimer -= Time.deltaTime;
        }

        private void Fire()
        {
            var bulletGo = Instantiate(bulletPrefab, transform.position, Quaternion.identity, bulletsParent);
            var bulletMover = bulletGo.GetComponent<MoveForward>();

            if (bulletMover)
                bulletMover.Speed = fireSpeed;

            Destroy(bulletGo, bulletAutodestroyTime);
        }
    }
}
