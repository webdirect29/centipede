﻿using UnityEngine;

namespace Centipede
{
    public class PlayerMoveBounds : ScreenBounds
    {
        [SerializeField, Range(0, 1)]
        private float topAreaBoundPercentage = 0.33f; //Percentage of the screen where the player could move (Player's move area). Also restricts mushrooms to be placed here

        private float topAreaBound;

        private void OnValidate()
        {
            topAreaBound = Utils.VerticalDiameter - (topAreaBoundPercentage * Utils.VerticalDiameter);
        }

        public override float TopBound => Utils.TopBound - topAreaBound;
    }
}