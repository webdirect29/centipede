﻿using UnityEngine;

namespace Centipede
{
    [RequireComponent(typeof(PlayerMoveBounds))]
    public class PlayerMover : MoveController
    {
        protected override bool CanMove => Input.GetButton("Horizontal") || Input.GetButton("Vertical");

        private void Awake()
        {
            Init();
        }

        protected override void Init()
        {
            SetStartPosition();
        }

        private void SetStartPosition() => transform.position = GetClampedPosition(Vector3.down);

        private void Update()
        {
            if (CanMove)
                Move(CalculateMovePosition());
        }

        protected override void Move(Vector3 position)
        {
            transform.position = position;
        }

        protected override Vector3 CalculateMovePosition()
        {
            var xMove = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
            var yMove = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

            var position = transform.position += new Vector3(xMove, yMove, 0);
            return GetClampedPosition(position);
        }

        private Vector3 GetClampedPosition(Vector3 position)
        {
            #if UNTIY_EDITOR
            if (bounds.OverlapsTop(position.y))
                position.y = bounds.TopBound;
            #elif UNITY_STANDALONE
            var topBorder = 0 - Utils.VerticalRadius / 3;

            if (position.y > topBorder)
                position.y = topBorder;
            #endif

            if (bounds.OverlapsBottom(position.y))
                position.y = bounds.BottomBound;

            if (bounds.OverlapsRight(position.x))
                position.x = bounds.RightBound;

            if (bounds.OverlapsLeft(position.x))
                position.x = bounds.LeftBound;

            return position;
        }
    }
}
