﻿
namespace Centipede
{
    public static class Tags
    {
        public const string BulletTag = "Bullet";
        public const string MushroomTag = "Mushroom";
    }
}