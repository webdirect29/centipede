﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Centipede
{
    public static partial class Utils //Class that store all information about Screen dimensions/resolutions and has a relevant functions
    {
        public static float Ratio { get; private set; }
        public static float ScreenHorizontalOffset { get; private set; }
        public static float ScreenVerticalOffset { get; private set; }
        public static float VerticalRadius { get; private set; }
        public static float HorizontalRadius { get; private set; }
        public static float TopBound { get; private set; }
        public static float BottomBound { get; private set; }
        public static float RightBound { get; private set; }
        public static float LeftBound { get; private set; }
        public static float VerticalDiameter { get; private set; }
        public static float HorizontalDiameter { get; private set; }
        public static float HorizontalLength { get; private set; }
        public static float VerticalLength { get; private set; }
        public static float StepSize => 0.08f;
        public static int ColomnsCount { get; private set; }
        public static int RawsCount { get; private set; }

        public static HashSet<Vector3> allPlacePositions;

        static Utils()
        {
            Init();       
        }

        public static void Init()
        {
            //Screen.width / Screen.height is working incorrectly in the Unity editor, 
            //To calculate a screen ratio, we should make some trick.

            #if UNITY_EDITOR
            string[] res = UnityStats.screenRes.Split('x');
            var width = int.Parse(res[0]);
            var height = int.Parse(res[1]);
            Ratio = (float)width / (float)height;
            #else
            Ratio = (float)Screen.width / (float)Screen.height;
            #endif

            //Initialize all properties to make a cache of them values
            VerticalRadius = Camera.main.orthographicSize;
            HorizontalRadius = VerticalRadius * Ratio;
            ScreenVerticalOffset = VerticalRadius * StepSize / 2;
            ScreenHorizontalOffset = HorizontalRadius * StepSize / 2;
            TopBound = VerticalRadius - ScreenVerticalOffset;
            BottomBound = -VerticalRadius + ScreenVerticalOffset;
            RightBound = HorizontalRadius - ScreenHorizontalOffset;
            LeftBound = -HorizontalRadius + ScreenHorizontalOffset;
            VerticalDiameter = VerticalRadius * 2;
            HorizontalDiameter = HorizontalRadius * 2;
            HorizontalLength = HorizontalDiameter - ScreenHorizontalOffset * 2;
            VerticalLength = VerticalDiameter - ScreenVerticalOffset * 2;
            ColomnsCount = Mathf.RoundToInt(HorizontalDiameter / StepSize);
            RawsCount = Mathf.RoundToInt(VerticalDiameter / StepSize);

            allPlacePositions = CalculateAllPlacePositions();
        }

        public static HashSet<Vector3> CalculateStepPositions(ScreenBounds bounds)
        {
            var positionsSet = new HashSet<Vector3>();

            for (int raw = 0; raw < RawsCount; raw++)
            {
                for (int colomn = 0; colomn < ColomnsCount; colomn++)
                {
                    var position = CalculatePlacementPosition(colomn, raw);

                    if (bounds.OverlapsPosition(position))
                        positionsSet.Add(position);
                }
            }
            return positionsSet;
        }

        public static HashSet<Vector3> CalculatePlacementPositions(ScreenBounds bounds)
        {
            var positionsSet = new HashSet<Vector3>();

            for (int raw = 0; raw < RawsCount; raw++)
            {
                for (int colomn = 0; colomn < ColomnsCount; colomn++)
                {
                    var position = CalculatePlacementPosition(colomn, raw);

                    if (bounds.OverlapsPositionWithOffset(position))
                        positionsSet.Add(position);
                }
            }
            return positionsSet;
        }

        public static Vector3 GetClosestPosition(Vector3 position, HashSet<Vector3> positionsSet)
        {
            return positionsSet.OrderBy(p => (p - position).sqrMagnitude).First();
        }

        public static Vector3 GetClosestPosition(Vector3 position, HashSet<Vector3> positionsSet, Vector2 offset)
        {
            var xPos = position.x + (offset.x * StepSize);
            var yPos = position.y + (offset.y * StepSize);
            var offsetPosition = new Vector3(xPos, yPos, 0);
            return GetClosestPosition(offsetPosition, positionsSet);
        }

        public static Vector3 CalculatePlacementPosition(int horizontalOffsetInd, int verticalOffsetInd)
        {
            var xOffset = ScreenHorizontalOffset;
            var yOffset = ScreenVerticalOffset;
            var position = new Vector3(-HorizontalRadius + xOffset, VerticalRadius - yOffset) + new Vector3(StepSize * horizontalOffsetInd, -StepSize * verticalOffsetInd);
            return position;
        }

        public static HashSet<Vector3> CalculateAllPlacePositions()
        {
            var positions = new HashSet<Vector3>();

            for (int raw = 0; raw < RawsCount; raw++)
            {
                for (var colomn = 0; colomn < ColomnsCount; colomn++)
                    positions.Add(CalculatePlacementPosition(colomn, raw));
            }
            return positions;
        }

        public static HashSet<Vector3> AllPossiblePlacePositions
        {
            get
            {
                if (allPlacePositions == null)
                    allPlacePositions = CalculateAllPlacePositions();

                return allPlacePositions;
            }
        }
    }
}
