﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Centipede
{
    public static partial class Utils
    {
        private static System.Random random = new System.Random();

        public static T CreateObjecOfType<T>(GameObject prefab, Vector3 position, Transform parent, string name = null) where T : Component
        {
            var go = GameObject.Instantiate(prefab, position, Quaternion.identity, parent) as GameObject;
            go.name = name != null ? name : go.name;
            return go.GetComponent<T>();
        }

        public static int GetRandomIntValue(int minValue, int maxValue, bool inclusive = true)
        {
            if (inclusive)
                maxValue++;

            return Random.Range(minValue, maxValue);
        }

        public static HashSet<int> GetSetOfRandomIntValues(int count, int minValue, int maxValue)
        {
            var set = new HashSet<int>();

            while (set.Count < count)
                set.Add(GetRandomIntValue(minValue, maxValue, false));

            return set;
        }
    }
}
