﻿using System;
using UnityEngine;

namespace Centipede
{
    public class ColisionDetector : MonoBehaviour
    {
        public event Action<Collider2D> Collided;

        private void OnTriggerEnter2D(Collider2D other)
        {
            Collided?.Invoke(other);
        }
    }
}

