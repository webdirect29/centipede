﻿using UnityEngine;

namespace Centipede
{
    public abstract class MoveController : MonoBehaviour
    {
        [SerializeField, Range(0.1f, 5)]
        protected float moveSpeed = 1;

        [SerializeField]
        protected ScreenBounds bounds;

        protected abstract void Move(Vector3 position);
        protected abstract Vector3 CalculateMovePosition();

        protected void MoveToNextPositon()
        {
            Move(CalculateMovePosition());
        }

        protected abstract void Init();
        protected abstract bool CanMove { get; }
    }
}