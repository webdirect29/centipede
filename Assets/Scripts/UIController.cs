﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Centipede
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private GameObject gameOverGo;

        private void Awake()
        {
            gameOverGo.SetActive(false);
            GameController.GameStateChanged += OnGameOver;
        }

        private void OnDestroy()
        {
            GameController.GameStateChanged -= OnGameOver;
        }

        public void OnGameOver(GameState state)
        {
            gameOverGo.SetActive(true);
        }

        public void OnPlayAgainPressed()
        {
            GameController.Restart();
        }
    }
}
