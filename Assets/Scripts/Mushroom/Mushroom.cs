﻿using System;
using UnityEngine;

namespace Centipede
{
    [RequireComponent(typeof(ColisionDetector))]
    public class Mushroom : Creature
    {
        protected override int InitHealth => 4;
    }
}