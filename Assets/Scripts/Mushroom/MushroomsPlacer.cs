﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Centipede
{
    public class MushroomsPlacer : MonoBehaviour
    {
        //Mushroom could be placed in a two ways. Randomly or Statically. To to use any change placerType Parameter at the inspector.
        //To place mushrooms statically you need to drag mushoom prefab to the Scene View

        [SerializeField]
        private MushroomPlacementType placerType;

        [SerializeField]
        private Transform mushroomsParent;

        [SerializeField, Range(1, 5), Header("Range of mushrooms in colomn")]
        private int minCountInColomn;

        [SerializeField, Range(1, 5)]
        private int maxCountInColomn;

        [SerializeField]
        private PlayerMoveBounds bounds;

        private HashSet<Vector3> placePositions;

        private MushroomPlacementType selectedPlacerType;
        public MushroomPlacementType PlacerType => placerType;

        private void OnValidate()
        {
            if (selectedPlacerType == placerType)
                return;

            #if UNITY_EDITOR
            if (EditorApplication.isPlaying)
                return;
            #endif

            selectedPlacerType = placerType;

            #if UNITY_EDITOR
            DestroyMushroomsImmediate();
            #endif

            if (placerType == MushroomPlacementType.Static)
            {
                Debug.LogWarning("<b>You can place mushrooms statically by dropping mushroom prefabs in a scene view</b>");
            }
        }

        #if UNITY_EDITOR
        void OnDrawGizmosSelected()
        {
            DrawPlacementPositions();
        }

        private void DrawPlacementPositions()
        {
            Gizmos.color = Color.yellow;

            var positions = Utils.CalculatePlacementPositions(bounds);

            foreach (var pos in positions)
                Gizmos.DrawWireSphere(pos, 0.01f);
        }

        private void DestroyMushroomsImmediate()
        {
            if (mushroomsParent != null)
            {
                var tempArray = new GameObject[mushroomsParent.transform.childCount];

                for (int i = 0; i < tempArray.Length; i++)
                    tempArray[i] = mushroomsParent.transform.GetChild(i).gameObject;

                EditorApplication.delayCall += () =>
                {
                    foreach (var child in tempArray)
                        DestroyImmediate(child);
                };
            }
        }
        #endif

        private HashSet<int> CalculateRandomCountFromRange(int minCount, int maxCount, int maxValue)
        {
            return Utils.GetSetOfRandomIntValues(Utils.GetRandomIntValue(minCount, maxCount), 0, maxValue);
        }

        public List<Mushroom> PlaceRandomly(GameObject mushroomPrefab)
        {
            var mushrooms = new List<Mushroom>();

            for (int raw = 0; raw < Utils.RawsCount; raw++)
            {
                var colomns = CalculateRandomCountFromRange(minCountInColomn, maxCountInColomn, Utils.ColomnsCount);

                foreach (var colomn in colomns)
                {
                    var position = Utils.CalculatePlacementPosition(colomn, raw);

                    #if UNITY_EDITOR
                    if (bounds.OverlapsPositionWithOffset(position))
                    #elif UNITY_STANDALONE
                    if (position.y > 0 - Utils.VerticalRadius / 3)                    
                    #endif
                        mushrooms.Add(Utils.CreateObjecOfType<Mushroom>(mushroomPrefab, position, mushroomsParent, mushroomPrefab.name));

                    if (bounds.OverlapsPositionWithOffset(position))
                        mushrooms.Add(Utils.CreateObjecOfType<Mushroom>(mushroomPrefab, position, mushroomsParent, mushroomPrefab.name));
                }
            }
            return mushrooms;
        }

        #if UNITY_EDITOR
        private List<Mushroom> PlaceStatically(GameObject mushroomGo, ScreenBounds bounds)
        {
            if (EditorApplication.isPlaying)
                return FindObjectsOfType<Mushroom>().ToList();

            var mushroom = mushroomGo.GetComponent<Mushroom>();
            var positions = Utils.CalculatePlacementPositions(bounds);
            var closestPosition = Utils.GetClosestPosition(mushroom.Position, positions);

            if (bounds.OverlapsPosition(closestPosition))
            {
                mushroom.transform.parent = mushroomsParent;
                mushroom.transform.position = closestPosition;
            }
            return new List<Mushroom>() { mushroom };
        }
        #endif

        public Mushroom PlaceOne(GameObject mushroomPrefab, Vector3 position)
        {
            var closestPosition = Utils.GetClosestPosition(position, Utils.AllPossiblePlacePositions);
            return Utils.CreateObjecOfType<Mushroom>(mushroomPrefab, closestPosition, mushroomsParent, mushroomPrefab.name);
        }

        public List<Mushroom> Place(GameObject mushroom)
        {
            if (PlacerType == MushroomPlacementType.Random)
            {
                return PlaceRandomly(mushroom);
            }
            #if UNITY_EDITOR
            else if (PlacerType == MushroomPlacementType.Static)
            {
                return PlaceStatically(mushroom, bounds);
            }
            #endif
            return null;
        }
        private HashSet<Vector3> PlacePositions
        {
            get
            {
                if (placePositions == null)
                    placePositions = Utils.CalculatePlacementPositions(bounds);

                return placePositions;
            }
        }
    }
}
