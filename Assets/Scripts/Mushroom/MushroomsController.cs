﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Centipede
{
    [RequireComponent(typeof(MushroomsPlacer))]
    public class MushroomsController : GameElementController
    {
        [SerializeField]
        private MushroomsPlacer placer;

        [SerializeField]
        private GameObject mushroomPrefab;

        [SerializeField, Header("Mushroom Graphics")]
        private List<Sprite> mushroomStateSprites;

        public List<Mushroom> Mushrooms { get; private set; }

        public override void Init()
        {
            Mushrooms = placer.Place(mushroomPrefab);
            InitMushrooms(Mushrooms);
        }

        private void InitMushrooms(List<Mushroom> mushrooms)
        {
            foreach (var mushroom in mushrooms)
            {
                mushroom.HealthChanged += HeathChanged;
                mushroom.Dead += OnDead;
                SetStateGhaphic(mushroom.SpriteRenderer, mushroom.Health);
            }
        }

        private void HeathChanged(Creature mushroom)
        {
            SetStateGhaphic(mushroom.SpriteRenderer, mushroom.Health);
        }

        private void SetStateGhaphic(SpriteRenderer renderer, int health)
        {
            var sprInd = --health;

            if (sprInd < 0 || sprInd >= mushroomStateSprites.Count)
                return;

            renderer.sprite = mushroomStateSprites[sprInd];
        }

        private void OnDead(Creature creature)
        {
            DestroyMushroom(creature as Mushroom);
        }

        public void CreateMushroomWithoutRestrictions(Vector3 position) //Creating a mushroom even on the player's area
        {
            var mushroom = placer.PlaceOne(mushroomPrefab, position);
            InitMushrooms(new List<Mushroom>() { mushroom });
            Mushrooms.Add(mushroom);
        }

        private void DestroyMushroom(Mushroom mushroom)
        {
            mushroom.HealthChanged -= HeathChanged;
            mushroom.Dead -= OnDead;

            Mushrooms.Remove(mushroom);
            Destroy(mushroom.gameObject);
        }

        public bool IsPlaced(Vector3 position)
        {
            return Mushrooms.Any(m => m.Position == position);
        }
    }
}
