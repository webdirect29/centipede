﻿using System;
using UnityEngine;

namespace Centipede
{
    public class Bullet : MonoBehaviour
    {
        [HideInInspector]
        public int Damage = 1;

        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
