﻿using UnityEngine;

namespace Centipede
{
    public class MoveForward : MonoBehaviour
    {
        private float speed = 1;

        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        private void Update()
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
    }
}